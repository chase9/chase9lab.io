---
title: "Missing Emojis in KDE"
author: "Chase Christiansen"
date: 2021-02-11T09:20:53-06:00
subtitle: "How else to stay hip with the kids?"
tags:
  - KDE
  - Fixes
---

I've recently switched back to KDE after an extended stint on GNOME. I love the polish and workflow of GNOME, but the Plasma desktop is so packed with power-user features that it can be hard to stay away for long.

Of course, what I mean by power-user features is their excellent emoji support. Just like on macOS, Plasma has a button combo, `super+.`, which will bring up a searchable emoji picker. [Added in 2019](https://pointieststick.com/2019/12/08/this-week-in-kde-easy-emoji-input-and-more/), One quick keystroke and you're greeted with this:

![A blank emoji picker](/img/blank-picker.png)

_Well that's disappointing!_ What's going on here? I've ensured there are emoji fonts on my system, so what gives? Let's try launching the emoji picked via the command line to see if there's any debugging we can do.

![Launching emoji picker from cli](/img/troubleshoot-emoji-picker.png)

So it looks like our picker relies on a dictionary file to populate its emojis, but the file cannot be found. What's that about? As it turns out, the emoji picker relies on something called ibus. In the Linux world, ibus is a framework which allows for switching between keyboard locales an layouts. The KDE spin on Fedora doesn't include this package by default, so we will need to install it in order to get the picker working.

Run the command: `sudo dnf install ibus`

```bash
Installing:
 ibus         x86_64    1.5.23-2.fc33   updates   9.8 M
Installing dependencies:
 ibus-gtk2    x86_64    1.5.23-2.fc33   updates   27 k
 ibus-gtk3    x86_64    1.5.23-2.fc33   updates   28 k
 ibus-setup   noarch    1.5.23-2.fc33   updates   60 k
```

As soon as this is done, the ibus daemon will be run and your emoji dictionary should be generated. A quick `super+.` later and voilà!

![A properly populated emoji picker](/img/emojis-back-home.png)

Now if only there was a good tik-tok client for Linux 😂

p.s. It looks like this was [fixed in Fedora Rawhide (34)](https://src.fedoraproject.org/rpms/plasma-desktop/c/e0c085a7ca997b37ffc791714f10bc12b4a58658), so no need to raise a bug unless you're reading this in the future.
