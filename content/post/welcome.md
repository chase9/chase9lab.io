---
categories:
- meta
date: "2018-12-14T00:00:00Z"
excerpt: Congratulations on finding your way to my site!
tags:
- welcome
title: Welcome to my site!
---

Sometimes (rarely) I run across a problem I'm able to fix myself. When I encounter one of these you better believe I'll publish it here.

Also, I just need to make sure Jekyll is working with GitLab Pages properly. *shakes fist at sky*