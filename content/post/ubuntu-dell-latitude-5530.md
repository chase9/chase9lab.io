---
title: Linux on a 2022 Dell Latitude 5530
author: Chase Christiansen
date: 2022-10-01T03:16:00-06:00
lastmod: 2022-11-11T12:00:00-06:00
subtitle: Setting up Ubuntu to take advantage Dell's hardware
tags:
  - Ubuntu
  - Dell
  - Work
  - 0a5c:5843
  - fprintd
  - Latitude 5530
---

For my work, I've recently switch to using a Dell Latitude 5530 laptop with an Intel 12th gen processor. All-in-all it is a great machine and I've been enjoying using it. Along with the switch to this hardware came a switch to using Windows full-time from my macOS/Linux background. Windows has come a long way, and I have actually been enjoying using it! However, I recently caved and installed Linux on the machine in a second drive so I could continue ~~making things hard on myself~~ living in both worlds.

# Overview

I initially installed Fedora 35 with the KDE plasma desktop, my combination of choice on my desktop at home. I was immediately disappointed with the experience using Wayland on Plasma. Even with Intel integrated graphics, Plasma still has various papercuts when using Wayland. This, in addition to other issues, let me searching for alternatives. I realized that there were oem specific fixes available through Canonical's partnership with Dell; and so I settled on using Ubuntu 22.04 with the Gnome desktop. The experience has been great!

I'm a big fan of using BTRFS over EXT4, so I wanted to get Ubuntu working with BTRFS. The Ubiquity installer that Ubuntu is not as accommodating as the Fedora installer, in my opinion. This is especially true when trying to do any sort of advanced operation

The primary goal of this project is to create a functional laptop experience which mirrors my experience with Windows. I'm a tinkerer at heart, but as this laptop needs to be able to _do work_, I do not want it to break!

# First Impressions

## Power Draw

My initial impressions of both Fedora and Ubuntu, KDE and Gnome, were positive. I noticed that Fedora, both using KDE and Gnome, tended to use battery faster. I can only speculate why this would be, since Ubuntu didn't get any unique hardware entitlements out of the box.

## Wayland/Touchpad Gestures

As mentioned before, I had issues when using Wayland with KDE Plasma. The experience was usable, and I could get my work done, but the experience was not nearly as polished as using Gnome.

I am a huge fan of a focused workflow. I typically work with a single monitor, and one or two windows tiled per-desktop. Gnome on Wayland has excellent support for touchpad gestures. You can use three-fingers to get to the overview and to switch workspaces.

## Fingerprint Reader

Out of the box, the fingerprint reader did not work. I did get this working, which I will address in the workarounds section.

## Audio

Audio input and output worked out of the box. It appears both Ubuntu 22.04 and Fedora 35 are using Pipewire. Fedora is using the wireplumber session manager, which Ubuntu is using the built-in pipewire-session-manager. I didn't have a problem with either.

## Keyboard

The keyboard works flawlessly out of the box, including all function and shortcut keys.

One bit of unique behavior I noticed is that the microphone mute key only mutes the internal microphone. If you have your laptop docked to an external mic, _people will still be able to hear you even though the mic mute light is on_! Hope this can save you from some embarassment!

## Snaps vs Flatpak

I wanted to challenge myself to use Snaps only in order to get the genuine Ubuntu experience. There really isn't much of a difference between the two, with most of the applications I wanted being available on both. A couple of quick things to note:

### Speed

This has been talked about to death, but there is a noticeable difference in launch times with Snaps and everything else. Specifically, the Teams and Firefox snaps have a noticeable delay, although it's my understanding this is getting better all of the time.

### Integration

This one is hard to quantify, but snaps do _feel_ like they are more a part of the system than flatpaks do. On the first launch of your Ubuntu system, the software store is loaded up with the snap store. Typically with flatpaks, you will need to add flathub yourself. I also liked that Gnome settings has permissions management for snaps built in, as opposed to having to install something like [flatseal](https://github.com/tchx84/Flatseal) yourself.

I'm someone who likes auto-updates, so I didn't mind that snaps are auto-updating. I did notice there was some odd behavior with not being able to update the software center from the software center... This feels like an oversight.

# Fixes and Workarounds

## Enabling the Latitude 5530 fingerprint reader on Ubuntu 22.04

Out of the box, the fingerprint reader does not work with fprintd. Luckily, Dell has a driver created for the Broadcom 0a5c:5843 fingerprint reader that we can install manually. The driver is available out of [this repository](http://dell.archive.canonical.com/updates/pool/public/libf/libfprint-2-tod1-broadcom/). The file I downloaded was "libfprint-2-tod1-broadcom_5.6.016.0-0ubuntu1oem3_amd64.deb", **but you should check to see what the latest is**.

```
wget http://dell.archive.canonical.com/updates/pool/public/libf/libfprint-2-tod1-broadcom/libfprint-2-tod1-broadcom_5.6.016.0-0ubuntu1oem3_amd64.deb

sudo apt install ./libfprint-2-tod1-broadcom_5.6.016.0-0ubuntu1oem3_amd64.deb
```

Unfortunately, this didn't get the reader working for me. Looking into the journal for fprintd revealed a dependency on libcrypto1.1 which isn't available on Ubuntu 22.04. The bug for this is being tracked [here](https://bugs.launchpad.net/libfprint-2-tod1-broadcom/+bug/1991794)

`/libfprint-2-tod-1-broadcom.so: libcrypto.so.1.1: cannot open shared object file: No such file or directory`

It appears the older version of libcrypto is not available in the default Ubuntu 22.04 repositories. We can still install a deb manually by downloading it from the archives.

I downloaded "libssl1.1_1.1.1f-1ubuntu2.16_amd64.deb" from here: https://mirror.umd.edu/ubuntu/ubuntu/pool/main/o/openssl/

After installing this and restarting fprintd.service, everything started working! You can see by running `sudo systemctl status fprintd` and looking to see that your fingerprint reader is recognized.

Note: you may also have to enable fingerprint authentication by running `sudo pam-auth-update` and selecting fingerprint authentication. This ensures that the fprintd pam module is loaded into your authentication stack.

Update 2022-11-12: After suspend, there is an issue with fprintd communicating with the fingerprint reader.

```
Nov 12 10:36:41 chase-Latitude-5530 fprintd[95756]: Error: 0xb from cv_get_ush_ver()
Nov 12 10:36:44 chase-Latitude-5530 fprintd[95756]: In cvif_IsUshThere(), cv_get_ush_ver() status: (0xb)
Nov 12 10:36:48 chase-Latitude-5530 fprintd[95756]: In cvif_IsUshThere(), cv_get_ush_ver() status: (0xb)
Nov 12 10:36:52 chase-Latitude-5530 fprintd[95756]: In cvif_IsUshThere(), cv_get_ush_ver() status: (0xb)
Nov 12 10:36:56 chase-Latitude-5530 fprintd[95756]: In cvif_IsUshThere(), cv_get_ush_ver() status: (0xb)
Nov 12 10:37:00 chase-Latitude-5530 fprintd[95756]: In cvif_IsUshThere(), cv_get_ush_ver() status: (0xb)
Nov 12 10:37:01 chase-Latitude-5530 fprintd[95756]: Could NOT detect USH.
Nov 12 10:37:01 chase-Latitude-5530 fprintd[95756]:                 FwUpgradeError. Check Firmware Files or CID used or Hardware etc. Error: 0x1c
Nov 12 10:37:01 chase-Latitude-5530 fprintd[95756]: Ignoring device due to initialization error: An unspecified error occurred!
```

This causes delays in authentication. I've also noticed that when I boot into Windows the fingerprint reader (ControlVault) is upgraded, and then downgraded again when I boot into Linux. It looks like fprintd doesn't have downgrade protection, and that the firmware Ubuntu ships is older than what is available from Dell. Here's how I got fprintd the latest firmware[^fprintd].

1. Download the latest ControlVault firmware recommended for your machine from Dells website. The one I downloaded was Dell-ControlVault3-Driver-and-Firmware_88J36_WIN_5.9.8.17_A20.EXE.
2. Extract the exe with your unarchiver of choice.
3. Replace the files in `/var/lib/fprint/fw` with the files you extracted. The firmware files are located in `<extracted folder>/production/Windows10-x64/18356/Drivers/cv/firmware/`
4. Restart fprintd to begin upgrading your firmware. This can take a minute and can be monitored in your system journal.

[^fprintd]: I found the instructions for extracting the Windows firmware on this GitHub discussion. https://github.com/dsd/fprintd/issues/3#issuecomment-1124435469
