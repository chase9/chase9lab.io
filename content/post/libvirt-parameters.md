---
categories:
- virtualization
date: "2019-11-02T00:00:00Z"
excerpt: Here's a list of parameters I find important, along with what they do.
tags:
- passthrough
- libvirt
title: Libvirt Parameters
---

When browsing through other peoples' setups, you'll find a huge range of flags people set. Here's a list of what I find important, along with what they do. The table on contents will be organized by levels to represent where each config option should go. Note that there are many more flags to explore, should you be so inclined[^1].

# domain

## memory

## currentMemory

## memoryBacking

### hugepages
Specifying huge pages 

## cputune

## os

## features

### acpi

### apic

### hyperv

#### relaxed

#### vapic

#### spinlocks

#### vpindex

#### runtime

#### synic

#### stimer

#### reset

#### vendor_id

#### frequencies

#### reenlightenment

#### tlbflush

### kvm

#### hidden

### vmport

### ipapic

## cpu

### topology

### feature

[^1]: [Here's the libvirt XML reference for some light reading](https://libvirt.org/formatdomain.html)