---
categories:
- linux
date: "2019-01-02T00:00:00Z"
excerpt: Why it is that your GRUB changes don't seem to work.
tags:
- grub
- kde
- neon
- fixes
title: Repairing GRUB on KDE Neon
---

# Background
I recently capped off about two weeks of nasty computer trouble with being unable to get KDE Neon to boot on it’s own. GRUB would boot into it’s recovery shell, and I would have to use pesky *command line trickery*[^1] to get my system running. 

Now, normally this is a quick fix. A little GRUB housekeeping and `update-grub` should do the trick. However, the strangest things were happening. No matter my changes, nothing was taking. Always back to the recovery shell for me. I scrapped every inch of my file system, tried all the forum posts, but nothing was working. 

{% include figure image_path="https://media.giphy.com/media/vC2hIDeImcwVO/giphy.gif" alt="head banging" caption="Artists rendition of me for about 4 days. Source: https://giphy.com/gifs/vC2hIDeImcwVO" %}

# Let’s troubleshoot this bad boy

I found out the easiest way to get my system booted was to set the prefix and run the command `normal`. I checked my config files for this and wouldn’t you know they were _supposed_ to be running almost the same commands. In fact, when I ran the commands listed in my GRUB config from the GRUB recovery shell, my system booted fine!

*So what the heck’s the problem*

In the recovery shell my environment was always being set to something like `prefix=EFI/Ubuntu`, despite not running Ubuntu. Let’s cue a section break for dramatic effect[^2].

# Problem, meet solution

I eventually found out what was causing this problem, and it’s solution, in [some documentation from the Neon team](https://community.kde.org/Neon/InstallableImages#KDE_neon_UEFI_implementation_June_2016).

The problem, as it turns out, is that GRUB[^3] has paths for Ubuntu hard coded. **This is not overridable by the user** and would need to be solved by changing and recompiling GRUB.

The way that Neon team solved this is by patching the installer Neon uses to copy the GRUB config files from EFI/Neon to EFI/Ubuntu.

To solve my problem I replicated this behavior. Once I had a copy of EFI/Neon as EFI/Ubuntu, my system started booting normally again.

For me this brings up a few questions. Why is GRUB hardcoded? Why would Neon copy instead of move it’s boot folder[^4]? Does this affect other distributions[^5]?

All I know is, I’m happy to have a booting computer again.

[^1]: What exactly does `normal`, `initrd`, and `linux` even do? No one knows.
[^2]: It took a couple days of head banging to figure this out, let me have this.
[^3]: or at least the version of GRUB that KDE Neon uses, which is a derivative of Kubuntu GRUB, which is a derivative of Ubuntu GRUB.
[^4]: and avoid OCD users like me from deleting seemingly extra folders.
[^5]: I know, I know, KDE Neon isn’t a distro.