---
title: "Repairing the Windows Bootloader"
author: "Chase Christiansen"
date: 2023-02-04T09:13:45-06:00
subtitle: "or how I stopped worrying and started loving UUIDs"
tags:
  - Windows
  - UEFI
---

# Overview

Because I have a complete and utter disregard for my own free time, I've found myself moving Windows partitions around several times in the past month. There are two really fun things about this:

1. Moving Windows partitions is 99% sure to break your bootloader
2. Most search results for `my windows pc won't boot please fix` are either out of date or trying to sell you <span class="radical">shareware</span>
   Luckily, EFI systems are pretty straight forward in how you get from power button to OS. Basically, when you start your PC it will look for an EFI partition containing boot files. The EFI partition is typically a standard fat32 partition with a special boot flag on it. The boot files can get a little complex, but they're _basically_ pointers to the actual OS to boot. Knowing this (or if you're skimming the article because you hate reading), here are the prerequisites:

## Prerequisites

1. Your PC must be running in UEFI (not BIOS), which means your disks are GPT partitioned.
2. You have an EFI partition you want to use already formatted and set with the boot flag.

# Danger Will Robinson

These instructions only apply to GPT disks and a UEFI system. If you do this while running MBR/BIOS... I have no idea what will happen. We're going to be messing with disks here, so be careful and don't blame me if something gets messed up.

# Let's do it

1. Boot into the Windows recovery environment. This is the magical blue place you end up when Windows can't start and automatic repair fails. You can also get here by booting Windows PE or a Windows Installer and clicking Repair. Open a command prompt by clicking `Troubleshoot` > `Command Prompt`

![Windows Recovery Environment](/img/windows-re.png "Screenshot of the Windows Recovery Environment")

2. The next step is to mount the EFI partition so we can access it. We will do this using diskpart. Here is a screenshot and explanation of what I did:

![Diskpart](/img/diskpart.png "Screenshot using diskpart to assign a letter to the EFI partition")

```powershell
diskpart
# Take note of which disk is your boot disk
list disk
# Selected my disk from the previous command
sel disk 0
# You want to find your EFI partition
list vol
# I can tell Volume 2 is my EFI since it's the only FAT partition
sel vol 2
# Assign a letter to the partition so we can access it
assign letter=f
```

> `list vol` will not show your EFI partition unless you have the boot flag set on it. If you don't see your partition, use a tool like `gparted` to check if this flag is set.

3. With your EFI partition mounted, the last step is to copy the boot files from your Windows install into the EFI partition using the `bcdboot` command. If you've been following this guide to a tee, you will want to use this command here: `bcdboot c:\windows /s f: /f UEFI`
   - The path c:\windows points to our Windows install. Check that this path is correct for you.
   - The /s flag specifies our EFI partition we assigned a letter to earlier.
   - The /f flag says we are only interested in files needed for UEFI boot.
4. Once this is done, close the command prompt and reboot. If everything goes well you should be greeted by a welcome sight, <span class="radical">Ads on your login screen</span>

![Windows Lockscreen](/img/lockscreen-ads.png "Screenshot showing the Windows login screen with ads on it.")
