---
categories:
- real life
date: "2019-05-07T00:00:00Z"
tags:
- jobs
- people
title: Ghosting in the Job Market
---
This is less of a rant and more some observations I've noticed. In the past year I've had the opportunity to talk to people who are hiring and people who are seeking jobs. Often, each group would be frustrated with the other.

A term you'll hear a lot in modern dating is *ghosting*. For those who aren't familiar, ghosting is defined as "the practice of ending a personal relationship with someone by suddenly and without explanation withdrawing from all communication".[^1] However, it's clear that the practice of ghosting isn't limited to modern dating. It's becoming more common for employees and employers alike to practice ghosting.

First, I'll give you some anecdotes. A friend of mine works in the retail space. Lately, she's been complaining about someone she works with who stopped showing up to work. The business is at a loss because, while she wasn't a great worker to begin with, *they're unable to formally fire her as they can't contact her*.[^2]

Conversely, I have friends of mine seeking to enter the job market who have similar experiences with employers. They say it's not uncommon to only hear back from 1/4 to 1/3 of the jobs you apply to. Their method for applying to jobs has gone from applying to jobs they are actually interested in to casting the net as wide as possible in hopes of hearing back from a third of them.

Indeed, stories like these seem to be widespread. I am subscribed to [/r/dataisbeautiful](https://reddit.com/r/dataisbeautiful) on Reddit; and over the past year or so I've noticed an increase in posts validating these observations. People will post charts detailing their job searches[^posts][^like][^these], sometimes with *hundreds* of applications submitted. almost always they have dismal results.[^3]

So, what does all of this say about the world today? Have we lost a common respect for each other? Are there simply too many applicants where companies can't keep up? Perhaps I just expect too much formality out of a business and it's employees? I'll leave that up to someone smarter than myself to figure out!

[^1]: New Oxford American Dictionary.
[^2]: Of course, since she is paid hourly, it is more of an annoyance than anything.
[^posts]: [posts](https://old.reddit.com/r/dataisbeautiful/comments/866qcz/6_months_of_job_hunting_by_city_until_i_finally)
[^like]: [like](https://old.reddit.com/r/dataisbeautiful/comments/d622lv/moved_countries_heres_six_months_of_job_hunting)
[^these]: [these](https://old.reddit.com/r/dataisbeautiful/comments/dlqleq/two_month_job_search_as_a_newgraduate_registered)
[^3]: Granted, this could be an excellent example of selection bias.