---
categories:
- unix
date: "2020-02-28T00:00:00Z"
tags:
- ssh
- linux
- macos
title: Enhancing SSH
---
A [recent episode of Linux Unplugged](https://linuxunplugged.com/342) was talking about using certificates for SSH auth, and got me thinking about enhancing my usage of SSH. I stumbled upon this page with some good recommendations from the infosec folks at Mozilla:

https://infosec.mozilla.org/guidelines/openssh