---
title: Making the CloudFree Smart Bulb Warmer
author: Chase
date: 2023-11-27T03:20:00-06:00
lastmod: 2022-11-27T20:00:00-06:00
subtitle: Fake it 'till you make it!
tags:
  - Home Assistant
  - Adaptive Lighting
  - Tasmota
---

I own a couple of the CloudFree Smart Bulbs[^1] that I use with HomeAssistant and the awesome Adaptive Lighting[^2] custom component. This trifecta makes it so the lights in my home automatically adjust to emulate the sun throughout the day. If you have HomeAssistant and haven't set this up yet, stop reading this and go set it up!

My only complaint about these lights is that the warmest they say they go is 2000k, which is not quite warm enough for my tastes. Luckily, since these bulbs come running Tasmota, there is a workaround we can put into place ourselves! Since the bulb has RGB LEDs in it, we can tell the bulb to fall back to the red LEDs when the bulb is set to its warmest levels.

{{< warning title="This is a tip" >}}
**Warning:** These specific instructions only apply to my CloudFree Smart Bulb. Do not run these commands without making sure you know what you're doing first!
{{< /warning >}}

To start, make sure you have a web browser open to the bulbs management page and open the console. These are the commands I ran:

1. Enable VirtualCT with `setoption105 1`. This enables us to map which of the internal LEDs should map to a CT "warmth" level.
2. Create your mapping with this template: `VirtualCT {"CT":"xxxxxxxxxx","CT":"xxxxxxxxxx","CT":"xxxxxxxxxx"}`. The exact command I used is this: `VirtualCT {"200":"000000ff00","400":"00000000ff","500":"FF9D000000"}`.

   - `"200":"000000ff00"` means that at a CT of 200 (which is pretty cool/blue), only use the cool white LED in the bulb.
   - `"400":"00000000ff"` means that at a CT of 400 (which is pretty warm/red), only use the warm white LEF in the bulb.
   - `"500":"FF9D000000"` means that for the final 400-500 CT, stop using the warm white LED of the bulb and instead emulate the warm light using the RGB LEDs.

3. Test the bulb and make sure you have a preset that works for you. Once you do, copy the command you used to your clipboard. The next step will be to save this command to run at boot since the custom settings here are not saved to flash.[^3]
4. Create a rule to run your custom mapping command every time the light reboots:

```
Rule1 on Power1#Boot do VirtualCT {"200":"000000ff00","400":"00000000ff","500":"FF9D000000"} endon
```

5. Enable the rule:

```
rule1
```

[^1]: https://cloudfree.shop/product/cloudfree-smart-bulb-rgbcw/
[^2]: https://github.com/basnijholt/adaptive-lighting
[^3]: https://github.com/arendst/Tasmota/pull/10311
