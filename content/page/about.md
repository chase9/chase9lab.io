---
title: "About me"
comments: false
---

{{< columns >}}

![Chase's Headshot](/img/headshot_casual_thumb.jpg)

{{< column >}}

Welcome to my website! My name is Chase Christiansen and I am a software developer.

I graduated from the University of Saint Thomas, Minnesota in May of 2018 with my Bachelors of Science in Computer Science. After school I spent a little time traveling, and am now employed full-time with Millwork Development, LLC in Woodbury, Minnesota.

This site was made as a place to collect my thoughts, especially after solving any problems that have irked me. Enjoy.

{{< endcolumns >}}
